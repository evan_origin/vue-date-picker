/**
 * @description 全局工具类函数
 * @author Evan Lin (evan_origin@163.com)
 * @date 2020/8/29
 */

const getYearMonthDay = (date) => {
    let year = date.getFullYear()
    let month = date.getMonth()
    let day = date.getDate()
    return {year,month,day}
}

const getDate = (year, month, day) => {
    return new Date(year, month, day)
}

export {
    getYearMonthDay,
    getDate
}
